/* libraries*/

import de.voidplus.leapmotion.*;
import processing.serial.*;
import cc.arduino.*;
import oscP5.*;
import netP5.*;

/* Leap Motion variables */

LeapMotion leap;
ArrayList<PVector> points;
PVector hp;

/* Arduino variables */

Arduino arduino;

/* OSC variables */

OscP5 oscP5;
NetAddress myRemoteLocation;
int portToSend = 7401;


/* Local variables */
PVector lastPosition = new PVector(0, 0, 0);
int lastTime = millis();

void setup(){
  size(800,500, P3D);
  leap = new LeapMotion(this);
  points = new ArrayList<PVector>();
  
  arduino = new Arduino(this, "COM7", 57600);
 
  arduino.pinMode(9, Arduino.SERVO);
  arduino.pinMode(10, Arduino.SERVO);
  
  oscP5 = new OscP5(this, portToSend);
  myRemoteLocation = new NetAddress("169.254.16.46", portToSend);
}

void draw(){
  background(255);
  int fps = leap.getFrameRate();
  frameRate(fps);
  //Mains
  for(Hand hand : leap.getHands()){
   
   hp = hand.getPosition();

   //println( hp.x + " " + hp.y + " " + hp.z);

   float valx = constrain(hp.x, 0, 800);
   float valy = constrain(hp.y, 0, 400); 
   float valz = constrain(hp.z, 0, 90);
   
   lastPosition.x = valx;
   lastPosition.y = valy;
   lastPosition.z = valz;

  if(valx < 400){
     arduino.servoWrite(9, (int)map((constrain(valz, 10, 90)),10, 90, 90, 180)); 
  }else{
      arduino.servoWrite(10, (int)map((constrain(valz, 10, 90)),10, 90, 90, 180));
  }
  
   
   fill(0);
   ellipse(hp.x, hp.y, constrain(hp.z, 1,20), constrain(hp.z, 1, 20));
   //arduino.servoWrite(10, (int)map((constrain(valz, 10, 90)),10, 90, 90, 180));

   
   valx = map(constrain(hp.x, 0, 800), 0, 800, -1, 1);
   valy = map(constrain(hp.y, 0, 400), 0, 400, -1, 1); 
   valz = map(constrain(hp.z, 0, 90), 0, 90, -1, 1);

   
   oscSendPosition(valx,valy,valz);
     
  }
}
 
 
void oscSendPosition(float x, float y, float z){
 
 OscMessage handPositionMsg =  new OscMessage("/handPosition");
 
 handPositionMsg.add(x);
 handPositionMsg.add(y);
 handPositionMsg.add(z);
 
 
   
//  /* compute velocity */
//  PVector deltaP = new PVector();
//  deltaP.x = (int)h.getPosition().x - (int)lastPosition.x;
////  deltaP.y = (int)h.getPosition().y - (int)lastPosition.y;
////  deltaP.z = (int)h.getPosition().z - (int)lastPosition.z;
//   
//  lastPosition = h.getPosition();
//   
//  int deltaT = millis() - lastTime;
//  lastTime = millis();
//   
//  float velx = deltaP.x / deltaT;
//   
//  myMessage.add(velx);
////  myMessage.add(vel.y);
////  myMessage.add(vel.z);
   
  oscP5.send(handPositionMsg, myRemoteLocation);
   
  println("message sent !");
}
 
 
 

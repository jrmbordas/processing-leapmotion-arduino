# processing-leapmotion-arduino
This peace of code in processing uses leap motion informations to control a servo plugged on an arduino.

#Installation
You need to install two processing libraries in your default library folder :
- The LeapMotionForProcessing library found here : https://developer.leapmotion.com/libraries?q=processing
- The arduino library for processing found here : http://playground.arduino.cc/interfacing/processing
